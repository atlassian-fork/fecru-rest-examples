from setuptools import setup, find_packages

setup(name='fecru_rest',
      version='1.0',
      description="FishEye & Crucible REST Client",
      author='Maciej Swinarski',
      author_email='mswinarski@atlassian.com',
      url='https://bitbucket.org/atlassian/fecru-rest-examples/src/',
      license='',
      packages=['fecru_rest'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
            'requests>=2.8'
      ]
      )
