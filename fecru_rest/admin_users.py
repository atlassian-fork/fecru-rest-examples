import requests
import json
import config

from .common import *

# FishEye & Crucible example API for accessing /rest-service-fecru/admin/users endpoint
# See https://docs.atlassian.com/fisheye-crucible/latest/wadl/fecru.html#rest-service-fecru:admin:users for FeCru Users REST API docs.

def get_users(start=0, limit=100):
    return get_all_paged_results("admin/users", start, limit)


def get_user(name):
    path = "admin/users/%s" % name
    r = requests.get(url(path), auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def create_user(name, password, display_name, email, type=None, crucible=None):
    path = "admin/users"
    payload = {'name': name, 'password': password, 'displayName': display_name, 'email': email, 'type': type, 'crucible': crucible}
    headers = {'content-type': 'application/json'}
    r = requests.post(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 201)
    return r.json()


def update_user(name, new_password=None, new_display_name=None, new_email=None, new_type=None, new_crucible=None, new_timezone=None):
    path = "admin/users/%s" % name
    payload = {'password': new_password, 'displayName': new_display_name, 'email': new_email, 'type': new_type, 'crucible': new_crucible}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def delete_user(name):
    path = "admin/users/%s" % name
    r = requests.delete(url(path), auth=(config.user, config.password))
    return r.status_code == 204


def add_group_to_user(user, group):
    path = "admin/users/%s/groups" % user
    payload = {'name': group}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 204)


def delete_group_from_user(user, group):
    path = "admin/users/%s/groups" % user
    payload = {'name': group}
    headers = {'content-type': 'application/json'}
    r = requests.delete(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    return r.status_code == 204


def get_user_groups(user, start=0, limit=100):
    path = "admin/users/%s/groups" % user
    return get_all_paged_results(path, start, limit)










