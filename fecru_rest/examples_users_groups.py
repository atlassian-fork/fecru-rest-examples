import pprint

from .admin_groups import *
from .admin_users import *

# FishEye & Crucible REST examples
# Users and groups examples
def usersAndGroupsExample():
    example_user = 'example_user'
    example_group1 = 'example_group1'
    example_group2 = 'example_group2'

    # Get all users and print:
    # * number of users
    # * fist 5 emails sorted alphabetically
    users = get_users()
    print "Number of users: %d" % (len(users))
    print "First 5 emails: %s" % (", ".join(sorted(filter(None, map(lambda user: user.get('email'), users)), key=lambda s: s.lower())[:5]))

    # Try delete user
    print "Try deleting user \'%s\': %s" % (example_user, delete_user(example_user))

    # Create user
    print "Creating user: %s" % example_user
    create_user(example_user, "1234", "Example User", "example.user@gmail.com")

    # Get user by name
    print "Getting user:"
    pprint.pprint(get_user(example_user))

    # Updating user
    print "Updating user:"
    pprint.pprint(update_user(example_user, new_password='5678', new_display_name='New Example User'))

    # Try delete group
    print "Try deleting group \'%s\': %s" % (example_group1, delete_group(example_group1))
    print "Try deleting group \'%s\': %s" % (example_group2, delete_group(example_group2))

    # Create groups
    print "Creating groups: %s" % [example_group1, example_group2]
    create_group(example_group1)
    create_group(example_group2, ["built-in"], True)

    # Get group by name
    print "Getting group:"
    pprint.pprint(get_group(example_group1))

    # Updating group
    print "Updating group:"
    pprint.pprint(update_group(example_group1, new_admin=True))

    # Add user to group
    print "Adding user \'%s\' to groups: %s" % (example_user, [example_group1, example_group2])
    add_group_to_user(example_user, example_group1)
    add_user_to_group(example_user, example_group2)

    # List user's group
    print "Listing user \'%s\' groups: %s" % (example_user, ", ".join(map(lambda group: group['name'], get_user_groups(example_user))))

    # List group's users
    print "Listing group \'%s\' users: %s" % (example_group1, ", ".join(map(lambda user: user['name'], get_group_users(example_group1))))

    #Delete user from group
    print "Deleting user \'%s\' from group \'%s\': %s" % (example_user, example_group1, delete_user_from_group(example_user, example_group1))
    print "Deleting user \'%s\' from group \'%s\': %s" % (example_user, example_group1, delete_group_from_user(example_user, example_group1))

    # Delete user
    print "Deleting user \'%s\': %s" % (example_user, delete_user(example_user))

    print "Done users and groups examples."




