import pprint

from .admin_repositories import *

# FishEye & Crucible REST examples
# Users and groups examples
def repositoriesExample():

    example_repository_name_1 = 'example_repository_1'

    # Get all repositories and print:
    # * number of repositories
    # * fist 5 locations sorted alphabetically by name
    repositories = get_repositories()
    print "Number of repositories: %d" % (len(repositories))
    print "First 5 emails: %s" % (", ".join(sorted(filter(None, map(lambda repository: repository.get('name'), repositories)), key=lambda s: s.lower())[:5]))

    # Try stop repository
    print "Try stopping repository \'%s\': %s" % (example_repository_name_1, try_stop_repository(example_repository_name_1))
    # Try delete repository
    print "Try deleting repository \'%s\': %s" % (example_repository_name_1, try_delete_repository(example_repository_name_1))

    # Create repository
    new_git = {
        'type' : 'git',
        'name' : example_repository_name_1,
        'description' : "FeCru rest examples repository",
        'storeDiff' : True,
        'enabled' : False,
        'git' : {
            'location' : "git@bitbucket.org:atlassian/fecru-rest-examples.git",
            'auth' : {
                'authType' : 'key-generate'
            }
        }
    }
    print "Creating repository: %s" % new_git
    create_repository(new_git)

    update_git = {
        'description' : "FeCru rest examples repository",
        'enabled' : True
    }
    print "Update repository: %s with %s" % (example_repository_name_1, update_git)
    update_repository(example_repository_name_1, update_git)

    # Get repository by name
    print "Getting repository:"
    pprint.pprint(get_repository(example_repository_name_1))

    # Updating repository
    print "Updating repository:"
#    pprint.pprint(update_user(example_user, new_password='5678', new_display_name='New Example User'))

    print "Re-index source:"
    pprint.pprint(reindex_source_repository(example_repository_name_1))

    print "Re-index line count:"
    pprint.pprint(reindex_linecount_repository(example_repository_name_1))

    print "Re-index reviews:"
    pprint.pprint(reindex_reviews_repository(example_repository_name_1))

    print "Re-index search:"
    pprint.pprint(reindex_search_repository(example_repository_name_1))

    print "Re-index changeset discussion:"
    pprint.pprint(reindex_changeset_discussion_repository(example_repository_name_1))

    print "Re-scan metadata:"
    pprint.pprint(rescan_metadata_repository(example_repository_name_1, 400))

    print "Run incremental index:"
    pprint.pprint(incremental_index_repository(example_repository_name_1))

    print "Run full incremental index:"
    pprint.pprint(full_incremental_index_repository(example_repository_name_1))

    # Try stop repository
    print "Try stopping repository \'%s\': %s" % (example_repository_name_1, try_stop_repository(example_repository_name_1))
    # Try delete repository
    print "Try deleting repository \'%s\': %s" % (example_repository_name_1, try_delete_repository(example_repository_name_1))

    print "Done repositories examples."




