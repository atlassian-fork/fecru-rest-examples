import pprint

from .admin_projects import *


# FishEye & Crucible REST examples
# Project examples
def projectsExample():

    example_project_key_1 = 'EXAMPLE1'
    example_project_key_2 = 'EXAMPLE2'

    try_delete_project(example_project_key_1)
    try_delete_project(example_project_key_2)

    # Get all projects and print:
    # * number of projects
    # * fist 5 project keys and names
    projects = get_projects()
    print "Number of projects: %d" % (len(projects))
    print "First 5 projects: %s" % (", ".join(sorted(filter(None, map(lambda project: "%s - %s" % (project.get('key'), project.get('name')), projects)), key=lambda s: s.lower())[:5]))

    # Create project
    new_example_project = {
        'key' : example_project_key_1,
        'name' : 'Example project',
        'defaultRepositoryName' : 'example_repository_1',
        'storeFileContentInReview' : True,
        'permissionSchemeName' : 'example-permission-scheme2',
        'moderatorEnabled' : True,
        'defaultModerator' : 'example_user',
        'allowReviewersToJoin' : True,
        'defaultDurationInWeekDays' : 5,
        'defaultObjectives' : 'Please make sure feature branch builds are green.'
    }

    print "Try delete project: %s" % new_example_project.get('key')
    try_delete_project(new_example_project.get('key'))

    print "Creating project: %s" % new_example_project
    create_project(new_example_project)

    update_example_project = {
        'storeFileContentInReview' : False,
        'defaultDurationInWeekDays' : 10
    }
    print "Update project: %s with %s" % (example_project_key_1, update_example_project)
    update_project(example_project_key_1, update_example_project)

    print "Add default reviewer user:"
    add_default_reviewer_user(example_project_key_1, "example_user")

    print "Get default reviewer users:"
    pprint.pprint(get_default_reviewer_users(example_project_key_1))

    print "Add default reviewer group:"
    add_default_reviewer_group(example_project_key_1, "example_group1")
    add_default_reviewer_group(example_project_key_1, "example_group2")

    print "Get default reviewer groups:"
    pprint.pprint(get_default_reviewer_groups(example_project_key_1))

    print "Add allowed reviewer user:"
    add_allowed_reviewer_user(example_project_key_1, "example_user")

    print "Get allowed reviewer users:"
    pprint.pprint(get_allowed_reviewer_users(example_project_key_1))

    print "Add allowed reviewer group:"
    add_allowed_reviewer_group(example_project_key_1, "example_group1")
    add_allowed_reviewer_group(example_project_key_1, "example_group2")

    print "Get allowed reviewer groups:"
    pprint.pprint(get_allowed_reviewer_groups(example_project_key_1))

    # Get project by key
    print "Getting project:"
    pprint.pprint(get_project(example_project_key_1))

    # Create project
    new_example_project2 = {
        'key' : example_project_key_2,
        'name' : 'Example project 2',
        'defaultRepositoryName' : 'example_repository_1',
        'storeFileContentInReview' : True,
        'permissionSchemeName' : 'agile',
        'moderatorEnabled' : True,
        'defaultModerator' : 'example_user',
        'allowReviewersToJoin' : True,
        'defaultDurationInWeekDays' : 5,
        'defaultObjectives' : 'Please make sure feature branch builds are green.'
    }

    print "Try delete project: %s" % new_example_project2.get('key')
    try_delete_project(new_example_project2.get('key'))

    print "Creating project: %s" % new_example_project2
    create_project(new_example_project2)

    print "Move reviews from project: %s to project %s" % (example_project_key_1, example_project_key_2)
    move_reviews(example_project_key_1, example_project_key_2)

    print "Try delete project: %s" % example_project_key_1
    try_delete_project(example_project_key_1)

    print "Done projects examples."




