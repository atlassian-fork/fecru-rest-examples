#!/usr/bin/env python

import logging

from .examples_users_groups import *
from .examples_repositories import *
from .examples_projects import *
from .examples_permission_schemes import *


def main():
    logging.basicConfig(level=logging.DEBUG)
#     config.url =
#     config.user =
#     config.password =
    usersAndGroupsExample()
    repositoriesExample()
    permissionSchemesExample()
    projectsExample()


if __name__ == '__main__':
    main()
