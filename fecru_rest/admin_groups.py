import requests
import json
import config

from .common import *

# FishEye & Crucible example API for accessing /rest-service-fecru/admin/groups endpoint
# See https://docs.atlassian.com/fisheye-crucible/latest/wadl/fecru.html#rest-service-fecru:admin:groups for FeCru Groups REST API docs.

def get_groups(start=0, limit=100):
    return get_all_paged_results("admin/groups", start, limit)


def get_group(name):
    path = "admin/groups/%s" % name
    r = requests.get(url(path), auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def create_group(name, types=None, admin=None):
    path = "admin/groups"
    payload = {'name': name, 'types': types, 'admin': admin}
    headers = {'content-type': 'application/json'}
    r = requests.post(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 201)
    return r.json()


def update_group(name, new_types=None, new_admin=None):
    path = "admin/groups/%s" % name
    payload = {'types': new_types, 'admin': new_admin}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def delete_group(name):
    path = "admin/groups/%s" % name
    r = requests.delete(url(path), auth=(config.user, config.password))
    return r.status_code == 204


def add_user_to_group(user, group):
    path = "admin/groups/%s/users" % group
    payload = {'name': user}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 204)


def delete_user_from_group(user, group):
    path = "admin/groups/%s/users" % group
    payload = {'name': user}
    headers = {'content-type': 'application/json'}
    r = requests.delete(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    return r.status_code == 204


def get_group_users(group, start=0, limit=100):
    path = "admin/groups/%s/users" % group
    return get_all_paged_results(path, start, limit)
