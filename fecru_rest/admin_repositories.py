import requests
import json
import config
from requests.exceptions import RequestException
from .common import *

# FishEye & Crucible example API for accessing /rest-service-fecru/admin/repositories endpoint
# See https://docs.atlassian.com/fisheye-crucible/latest/wadl/fecru.html#rest-service-fecru:admin:repositories for FeCru Repositories REST API docs.


def get_repositories(start=0, limit=100):
    return get_all_paged_results("admin/repositories", start, limit)


def get_repository(name):
    path = "admin/repositories/%s" % name
    r = requests.get(url(path), auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def create_repository(new_repository):
    path = "admin/repositories"
    headers = {'content-type': 'application/json'}
    r = requests.post(url(path), data=json.dumps(new_repository), headers=headers, auth=(config.user, config.password))
    print r.status_code
    assert(r.status_code == 201)
    return r.json()


def update_repository(name, update_repository):
    path = "admin/repositories/%s" % name
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(update_repository), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def delete_repository(name):
    path = "admin/repositories/%s" % name
    r = requests.delete(url(path), auth=(config.user, config.password))
    return r.status_code == 204

def try_delete_repository(name, status_code=202):
    try:
        delete_repository(name)
    except RequestException:
        pass

def start_repository(name, status_code=202):
    return action_on_repository(name, "start", status_code)

def stop_repository(name, status_code=202):
    return action_on_repository(name, "stop", status_code)

def try_stop_repository(name, status_code=202):
    try:
        stop_repository(name, status_code)
    except RequestException:
        pass
    except AssertionError:
        pass

def reindex_source_repository(name, status_code=202):
    return action_on_repository(name, "reindex-source", status_code)

def reindex_linecount_repository(name, status_code=202):
    return action_on_repository(name, "reindex-linecount", status_code)

def rescan_metadata_repository(name, status_code=202):
    return action_on_repository(name, "rescan-metadata", status_code)

def incremental_index_repository(name, status_code=202):
    return action_on_repository(name, "incremental-index", status_code)

def full_incremental_index_repository(name, status_code=202):
    return action_on_repository(name, "full-incremental-index", status_code)

def reindex_reviews_repository(name, status_code=202):
    return action_on_repository(name, "reindex-reviews", status_code)

def reindex_search_repository(name, status_code=202):
    return action_on_repository(name, "reindex-search", status_code)

def reindex_changeset_discussion_repository(name, status_code=202):
    return action_on_repository(name, "reindex-changeset-discussion", status_code)

def action_on_repository(name, action, status_code):
    path = "admin/repositories/%s/%s" % (name, action)
    headers = {'content-type': 'application/json'}

    r = requests.put(url(path), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == status_code)



